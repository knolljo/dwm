# dwm

**d**ynamic **w**indow **m**anager

## Installation

1. Download the repository.

```console
git clone https://git.suckless.org/dwm
cd dwm
make
```

2. Individualize the config.

3. Compile and install the binary.

```console
sudo make clean install
```

4. Start dwm from a tty with [*startx*](https://wiki.archlinux.org/title/Startx) or [*sx*](https://github.com/Earnestly/sx), with sx being more suckless.

This needs an init-file. For example:

```shell
#!/bin/sh

setxkbmap -option caps:escape &  # Change capslock to escape key
xset r rate 215 65 &  # Change typematic rate and delay (first number is delay in ms, second is rate in Hz)
pipewire &  # Following three lines are for the audio-server
pipewire-pulse &
wireplumber &

# Windowmanager
exec dwn
```

## Patching

Download the patch-file from [suckless.org](https://dwm.suckless.org/patches/).

```console
patch -p1 < path/to/patch.diff
```

If something fails:

- *.rej -> look into .rej and try to fix the mentioned lines in the main-file (without .rej suffix)

## Keymaps

Mod = SuperKey

### Windows

| Key   | Action             |
|-------|--------------------|
| Mod+q | kill client (quit) |
| Mod+Shift+Q | kill xorg-server |
| Mod+b | togglebar (titlebar, tags, clock) |
| Mod+j | next window |
| Mod+k | previous window |
| Mod+f | cycle through layouts |
| Mod+Space | Make active window master|
| Mod+l/h | change master size |
| Mod+o | increment master (+1 master) |
| Mod+Shift+o | decrement master (-1 master) |
| Mod+Shift+Space | tile a floating window |
| Mod+{1..9} | change tag (desktops) |
| Mod+0 | all tags |
| Mod+Shift+0 | make window active on all tags |
| Mod+. | focus next monitor |
| Mod+, | focus previous monitor |

### Programs

| Key        | Action   |
|------------|----------|
| Mod+Return | terminal |
| Mod+Shift+Return | quake terminal |
| Mod+D | dmenu_run |
| Mod+w | browser (web) |
| Mod+Shift+w | secondary browser (web) |
| Mod+e | file-explorer (explorer) |
| Mod+r | task manager and system monitor |
| Print | screenshot active window |
| Mod+Print | screenshot area |
| Mod+u | password-manager |
| Mod+Ctrl+. | emoji selector |
| Mod+Shift+a | music player |

### Powerkeys

| Key        | Action   |
|------------|----------|
| Alt+Ctrl+s | shutdown |
| Alt+Ctrl+r | reboot |
| Alt+Ctrl+l | logout |
| Alt+Ctrl+h | hibernate |
| Alt+Ctrl+m | screensaver  |

