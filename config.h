#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 0;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = {"ComicCode Nerd Font:size=12", "monospace:size=12" };
static const char col_gray1[]       = "#000000";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#7f7f7f";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#000000";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_cyan },
	[SchemeSel]  = { col_gray4, col_cyan,  col_gray4  },
};

/* tagging */
static const char *tags[] = { "", "爵", "", "", "", "", "", "", "" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor    float x,y,w,h         floatborderpx  scratchtag*/
	{ "Gimp",     NULL,       NULL,       0,            1,           -1,        50,50,500,500,        5,          0 },
	{ "Firefox",  NULL,       NULL,       1 << 8,       0,           -1,        50,50,500,500,        5,          0 },
	{ NULL,       NULL,       "mkfloat",  0,            1,           -1,        150,50,1620,980,      0,          0 },
	{ NULL,       NULL,   "scratchpad",   0,            1,           -1,        0,0,1920,540,         0,         's' },
	{ NULL,       NULL,   "filemanager",  0,            1,           -1,        150,50,1620,980,      0,        'f' },
	{ NULL,       NULL,   "taskmanager",   0,           1,           -1,        150,100,1620,930,      0,        't' },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ " T ",      tile },    /* first entry is default */
	//{ " F ",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
#define TERMINAL "alacritty"
#define BROWSER "librewolf"
static const char *termcmd[]  = { TERMINAL, NULL };
static const char *browser[]  = { BROWSER, NULL };
static const char *sec_browser[]  = { BROWSER, "-P", "default", NULL };
static const char *scratchterm[] = {"s", TERMINAL, "-tscratchpad", NULL}; 
static const char *explorer[] = {"f", TERMINAL, "-tfilemanager", "-elfu", NULL}; 
static const char *taskmanager[] = {"t", TERMINAL, "-ttaskmanager", "-ebtm", NULL}; 


static Key keys[] = {
	/* modifier                     key        function        argument */
	// Windows
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_o,      incnmaster,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_o,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
  { MODKEY,                       XK_space,  zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_q,      killclient,     {0} },
  { MODKEY,                       XK_f,      setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
  // Programs
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY|ShiftMask,             XK_Return, togglescratch,  {.v = scratchterm } },
	{ MODKEY,                       XK_w,      spawn,          {.v = browser } },
	{ MODKEY|ShiftMask,             XK_w,      spawn,          {.v = sec_browser } },
	{ MODKEY,                       XK_e,      togglescratch,  {.v = explorer } },
	{ MODKEY,                       XK_r,      togglescratch,  {.v = taskmanager } },
  { 0,                            XK_Print,  spawn,          SHCMD("scrot -u")},
  { MODKEY,                       XK_Print,  spawn,          SHCMD("scrot -s")},
  { MODKEY,                       XK_u,      spawn,          SHCMD("passtype")},
  { MODKEY,                       XK_d,      spawn,          SHCMD("dmenu_run")},
  { MODKEY|ControlMask,           XK_period, spawn,          SHCMD("emoji")},
  { MODKEY|ShiftMask,             XK_a,      spawn,          SHCMD("music")},
  { MODKEY,                       XK_m,      spawn,          SHCMD("dmenumount")},
  { MODKEY|ShiftMask,             XK_m,      spawn,          SHCMD("dmenuunmount")},
  // Audio
  { MODKEY, XF86XK_AudioLowerVolume, spawn,  SHCMD("mpc prev")},
  { MODKEY, XF86XK_AudioRaiseVolume, spawn,  SHCMD("mpc next")},
  { MODKEY, XF86XK_AudioMute,     spawn,     SHCMD("mpc toggle")},
  { MODKEY, XF86XK_AudioMicMute,  spawn,     SHCMD("mpc seek +5")},
  { 0, XF86XK_AudioLowerVolume,   spawn,     SHCMD("pamixer -d 2")},
  { 0, XF86XK_AudioRaiseVolume,   spawn,     SHCMD("pamixer -i 2")},
  { 0, XF86XK_AudioMute,          spawn,     SHCMD("pamixer -t")},
  { 0, XF86XK_AudioMicMute,       spawn,     SHCMD("pactl set-source-mute @DEFAULT_SOURCE@ toggle")},
  // Monitor
  { 0, XF86XK_MonBrightnessUp,    spawn,     SHCMD("backlight_control +2")},
  { 0, XF86XK_MonBrightnessDown,  spawn,     SHCMD("backlight_control -2")},
  // Powerkeys
  { Mod1Mask|ControlMask|ShiftMask, XK_r, 	spawn, 		 SHCMD("systemctl reboot")},
  { Mod1Mask|ControlMask|ShiftMask, XK_s, 	spawn, 		 SHCMD("systemctl poweroff")},
  { Mod1Mask|ControlMask|ShiftMask, XK_h, 	spawn, 		 SHCMD("systemctl suspend")},
  { Mod1Mask|ControlMask|ShiftMask, XK_l, 	spawn, 		 SHCMD("loginctl terminate-user $(whoami)")},
  { Mod1Mask|ControlMask|ShiftMask, XK_m, 	spawn, 		 SHCMD("xset dpms force suspend")},
	// Tags
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

